import React from "react";
import styles from "../Footer/Footer.module.css";
import logoLinkedIn from "../../images/linkedin.png";


const Footer = () => {
  return (
    <footer className={styles["topnav"]}>
      <div className={styles["footer-p"]}>
        <p>© 2022 Jocner Patiño</p>
      </div>
      <div className={styles["icons-media"]}>
      

        <a href="https://www.linkedin.com/in/jocner/" target={"blank"}>
          <img src={logoLinkedIn} alt="logoLinkedIn" />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
