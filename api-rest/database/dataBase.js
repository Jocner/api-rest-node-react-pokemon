const { Sequelize } = require("sequelize");
const dotenv = require("dotenv");

dotenv.config({ path: './enviroment.env' });

// const {
//   NODE_ENV,
//   DB_USER,
//   DB_PASSWORD,
//   DB_HOST,
//   DB_NAME,
// } = require("../config/dotenv");

// const {
//   NODE_ENV,
//   DB_USER,
//   DB_PASSWORD,
//   DB_HOST,
//   DB_NAME,
// } = require("../config/dotenv");

// const optionsDev = {
//   logging: false, // Disables logging
// };

// const optionsProd = {
//   logging: false, // Disables logging
//   dialect: "postgres",
//   protocol: "postgres",
//   dialectOptions: {
//     ssl: {
//       require: true,
//       rejectUnauthorized: false,
//     },
//   },
// };

// const sequelize = new Sequelize(
//   `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}/${DB_NAME}`,
//   NODE_ENV === "development" ? optionsDev : optionsProd
// );

// const sequelize = new Sequelize(
//   `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}/${DB_NAME}`,
// );

const sequelize = new Sequelize(
  process.env.DB_NAME, // db name,
  process.env.DB_USER, // username
  process.env.DB_PASSWORD, // password
  {
    host: "localhost",
    dialect: "postgres",
  }
);


sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((err) => {
    console.error("Unable to connect to the database:", err);
  });

module.exports = sequelize;
